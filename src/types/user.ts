export interface User {
    id?: number
    account?: string
    password?: string
    name?: string
}