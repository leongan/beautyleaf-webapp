export interface Tag {
    id: number
    tagName: string
    maxId: number
    step: number
    description: string
    updateTime: Date | string
    active: boolean,
    prefixId: number
}
