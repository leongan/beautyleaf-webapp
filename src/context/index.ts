import {useTagProvider} from "../hooks/useTag";
import {usePrefixProvider} from "../hooks/usePrefix";
import {useUserProvider} from "../hooks/useUser";

export const useProvider = () => {
    useTagProvider()
    usePrefixProvider()
    useUserProvider()
}