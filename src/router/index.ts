import {createRouter, createWebHashHistory} from 'vue-router'

const modules = import.meta.glob('../pages/**/*.vue')

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            redirect: '/tag'
        },
        {
            path: '/prefix',
            component: modules['../pages/prefix/PrefixList.vue'],
            meta: {
                label: '前缀管理',
            }
        },
        {
            path: '/tag',
            component: modules['../pages/tag/TagList.vue'],
            meta: {
                label: '标签管理',
            }
        },
    ]
})

export default router